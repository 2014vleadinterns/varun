import sys
import os
import tempfile
__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset", "getsize"]
__text__ = None
__blob__ = None
__size__ = None

def copytext(text,size):
    global __text__
    global __size__
    __size__=len(text)
    if(__size__ < size): 
       __text__ = text
    else :
       textmess=tempfile.TemporaryFile()
       __text__=textmess.name
       textmess.write(text)
       textmess.close()  
               
      
def copyblob(blob,size):
    global __blob__
    global __size__
    __size__=len(blob)
    if(__size__ < size): 
      __blob__ = blob
    else:
       blobmess=tempfile.TemporaryFile()
       __blob__=blobmess.name
       blobmess.write(text)
       blobmess.close()

def gettext():
    global __text__
    try: 
       __text__.seek(0)
      return __text__
    except:
       return __text__    

def getblob():
    global __blob__
    try:
        __blob__.seek(0)  
        return __blob__
    except:
        return __blob__
   
def getsize():
    global __size__
    return __size__

def reset():
    global __text__, __blob__, __
    __text__ = None
    __blob__ = None
    __size__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass

