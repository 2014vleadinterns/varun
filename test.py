# -*- coding: UTF-8 -*-
import clipboard 
#import Image
size=30
def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_copytext_clipboard():
        global size
        msg="kvk"
        clipboard.copytext(msg,size)
        if(len(msg) < size):
           assert(clipboard.gettext() != None)
           assert(clipboard.getblob() == None)        
        else:
           assert(clipboard.gettext() == None)
           assert(clipboard.getblob() == None)    
    
    def test_copyblob_clipboard():
        msg="rip"
        global size
        clipboard.copyblob(msg,size) 
        if(len(msg) < size):
           assert(clipboard.gettext() != None) 
           assert(clipboard.getblob() != None) 
        else:
           assert(clipboard.gettext() == None)
           assert(clipboard.getblob() != None)  
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)

    def test_copyblob1_clipboard():
        msg="rip"
        global size
        clipboard.copyblob(msg,size)
        if(len(msg) < size):
           assert(clipboard.gettext() == None)
           assert(clipboard.getblob() != None)
        else:
           assert(clipboard.gettext() == None)
           assert(clipboard.getblob() == None)         
  
    def test_copytext1_clipboard(): 
        msg="text"
        clipboard.copytext(msg,size) 
        if(len )
        assert(clipboard.gettext() != None)
        assert(clipboard.getblob() != None)      

    test_reset_clipboard()
    test_empty_clipboard()
    test_copytext_clipboard()
    test_copyblob_clipboard() 
    test_reset_clipboard()
    test_empty_clipboard()
    test_copyblob1_clipboard()
    test_copytext1_clipboard()   
    test_reset_clipboard()
    test_empty_clipboard()
run_clipboard_tests()

def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
