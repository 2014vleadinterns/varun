import tornado.ioloop
import tornado.web
import tornado.gen
import time
#import RequestHandler
#import http_client
class MyFormHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("helloworld")

class varunhandler(tornado.web.RequestHandler):
     def get(self):
	while(True):
          file = open("varun.txt" , "a+")
          file.write("hello world in the new file\n")
          file.write("and another line\n")
          file.write("created a file for varuntest request\n")
          file.close()
          break                  

class hanokhandler(tornado.web.RequestHandler):
     def get(self):
        time.sleep(5)	
        while(True):
           file=open("hanok.txt","a+") 
           file.write("hello world in the test\n")
           file.write("and another line\n")
           file.write("created a file for hanoktest request\n")     
           file.close()
           break              

class gentest(tornado.web.RequestHandler):
     def get(self):
        http_client = AsyncHTTPClient()
        response = yield gen.Task(http_client.fetch, "http://localhost:8888")
        do_something_with_response(response)
        self.write("template.html") 

 
if  __name__=="__main__":
  application = tornado.web.Application([
    (r"/", MyFormHandler),
    (r"/varuntest",varunhandler), 
    (r"/hanoktest",hanokhandler),
    (r"/kvk",gentest)
    ])
  
  application.listen(8888)
#  http_client = tornado.httpclient.AsyncHTTPClient()
#  http_client.fetch("http://127.0.0.1:8888",handle_request,method='POST')	
  tornado.ioloop.IOLoop.instance().start()


